﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Forms;

namespace XYZShell.CsHelper.WinFrom
{
    public class ListViewX:ListView
    {      
        /// <summary>
        /// 列表的数据源
        /// </summary>
        [DisplayName("数据")]
        [Description("设置或获取列表的数据")]
        public IList<object> DataSource { get; set; }
        /// <summary>
        /// 刷新数据及控件和自控件显示
        /// </summary>
        public override void Refresh()
        {
            this.Tag = DataSource;
            base.Refresh();
        }
        private void setData()
        {
            this.Items.Clear();
            foreach (object ditem in DataSource)
            {
                
            }
        }
       
                
    }
}
