﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace XYZShell.CsHelper.App
{
    public class Everything
    {
        const int EVERYTHING_OK = 0;
        const int EVERYTHING_ERROR_MEMORY = 1;
        const int EVERYTHING_ERROR_IPC = 2;
        const int EVERYTHING_ERROR_REGISTERCLASSEX = 3;
        const int EVERYTHING_ERROR_CREATEWINDOW = 4;
        const int EVERYTHING_ERROR_CREATETHREAD = 5;
        const int EVERYTHING_ERROR_INVALIDINDEX = 6;
        const int EVERYTHING_ERROR_INVALIDCALL = 7;

        [DllImport("Everything.dll")]
        public static extern int Everything_SetSearch(string lpSearchString);
        [DllImport("Everything.dll")]
        public static extern void Everything_SetMatchPath(bool bEnable);
        [DllImport("Everything.dll")]
        public static extern void Everything_SetMatchCase(bool bEnable);
        [DllImport("Everything.dll")]
        public static extern void Everything_SetMatchWholeWord(bool bEnable);
        [DllImport("Everything.dll")]
        public static extern void Everything_SetRegex(bool bEnable);
        [DllImport("Everything.dll")]
        public static extern void Everything_SetMax(int dwMax);
        [DllImport("Everything.dll")]
        public static extern void Everything_SetOffset(int dwOffset);

        [DllImport("Everything.dll")]
        public static extern bool Everything_GetMatchPath();
        [DllImport("Everything.dll")]
        public static extern bool Everything_GetMatchCase();
        [DllImport("Everything.dll")]
        public static extern bool Everything_GetMatchWholeWord();
        [DllImport("Everything.dll")]
        public static extern bool Everything_GetRegex();
        [DllImport("Everything.dll")]
        public static extern UInt32 Everything_GetMax();
        [DllImport("Everything.dll")]
        public static extern UInt32 Everything_GetOffset();
        [DllImport("Everything.dll")]
        public static extern string Everything_GetSearch();
        [DllImport("Everything.dll")]
        public static extern int Everything_GetLastError();

        [DllImport("Everything.dll")]
        public static extern bool Everything_Query();

        [DllImport("Everything.dll")]
        public static extern void Everything_SortResultsByPath();

        [DllImport("Everything.dll")]
        public static extern int Everything_GetNumFileResults();
        [DllImport("Everything.dll")]
        public static extern int Everything_GetNumFolderResults();
        [DllImport("Everything.dll")]
        public static extern int Everything_GetNumResults();
        [DllImport("Everything.dll")]
        public static extern int Everything_GetTotFileResults();
        [DllImport("Everything.dll")]
        public static extern int Everything_GetTotFolderResults();
        [DllImport("Everything.dll")]
        public static extern int Everything_GetTotResults();
        [DllImport("Everything.dll")]
        public static extern bool Everything_IsVolumeResult(int nIndex);
        [DllImport("Everything.dll")]
        public static extern bool Everything_IsFolderResult(int nIndex);
        [DllImport("Everything.dll")]
        public static extern bool Everything_IsFileResult(int nIndex);
        [DllImport("Everything.dll")]
        public static extern void Everything_GetResultFullPathName(int nIndex, StringBuilder lpString, int nMaxCount);
        [DllImport("Everything.dll")]
        public static extern void Everything_Reset();


        public static void Search(string searchTxt,Action<string> resultAc)
        {
            //int i = 0;
            const int bufszie = 260;
            StringBuilder sb = new StringBuilder(bufszie);
            Everything_SetSearch(searchTxt);
            Everything_Query();
            for (int i = 0; i < Everything_GetNumResults(); i++)
            {
                Everything_GetResultFullPathName(i, sb,bufszie);
                resultAc(sb.ToString());
            }
        }
        public static List<string> Search(string searchTxt)
        {
            List<string> filenames = new List<string>();
            Search(searchTxt, (string s) => { filenames.Add(s); });
            return filenames;
        }
    }
}
