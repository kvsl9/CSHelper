﻿using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XYZShell.CsHelper.DevExpressCore.Helper
{
    public static class TreeListHelper
    {
        /// <summary>
        /// 初始化TreeList默认设置
        /// </summary>
        /// <param name="treelist"></param>
        public static void InitNormal(this TreeList treelist)
        {
            TreeListColumn colId = new TreeListColumn();
            colId.Name = "colId";
            colId.Caption = "ID";
            colId.Visible = false;
            colId.FieldName = "ID";

            TreeListColumn colCaption = new TreeListColumn();
            colCaption.Name = "colCaption";
            colCaption.Caption = "Caption";
            colCaption.Visible = true;
            colCaption.FieldName = "Caption";
            colCaption.VisibleIndex = 0;

            TreeListColumn colVal = new TreeListColumn();
            colVal.Name = "colval";
            colVal.Caption = "value";
            colVal.Visible = false;
            colVal.FieldName = "value";
            treelist.Columns.AddRange(new TreeListColumn[] { colId, colCaption, colVal });

            treelist.OptionsView.ShowColumns = false;
            treelist.OptionsView.ShowFocusedFrame = false;
            treelist.OptionsView.ShowHorzLines = false;
            treelist.OptionsView.ShowVertLines = false;
            treelist.OptionsView.ShowIndicator = false;
            treelist.OptionsBehavior.Editable = false;
        }        
        /// <summary>
        /// 添加节点
        /// </summary>
        /// <param name="trelist">TreeList</param>
        /// <param name="id">节点ID</param>
        /// <param name="caption">节点显示文本</param>
        /// <param name="val">节点值</param>
        /// <param name="parentNode">父节点</param>
        /// <returns>添加的新节点</returns>
        public static TreeListNode AddNode(this TreeList trelist, string id, string caption, object val, TreeListNode parentNode)
        {
            return trelist.AppendNode(new object[] { id, caption, val }, parentNode);
        }
        /// <summary>
        /// 添加节点
        /// </summary>
        /// <param name="trelist">TreeList</param>
        /// <param name="id">节点ID</param>
        /// <param name="caption"></param>
        /// <param name="caption">节点显示文本</param>
        /// <param name="val">节点值</param>      
        /// <param name="isCheck">复选框是否选中</param>
        /// <param name="parentNode">父节点</param>
        /// <returns>添加的新节点</returns>
        public static TreeListNode AddNode(this TreeList trelist, string id, string caption, object val, bool isCheck, TreeListNode parentNode)
        {
            return trelist.AppendNode(new object[] { id, caption, val }, parentNode, isCheck ? System.Windows.Forms.CheckState.Checked : System.Windows.Forms.CheckState.Unchecked);
        }
        /// <summary>
        /// 添加节点
        /// </summary>
        /// <param name="treelist">TreeList</param>
        /// <param name="val">节点值</param>
        /// <param name="parentnode">父节点</param>
        /// <returns>添加的新节点</returns>
        public static TreeListNode AddNode(this TreeList treelist, object val, TreeListNode parentnode)
        {
            return treelist.AddNode(new object[] { val.ToString(), val.ToString(), val }, parentnode);
        }
        /// <summary>
        /// 添加节点
        /// </summary>
        /// <param name="treelist">TreeList</param>
        /// <param name="val">节点值</param>
        /// <param name="isCheck">复选框是否选中</param>
        /// <param name="parentnode">父节点</param>
        /// <returns>添加的新节点</returns>
        public static TreeListNode AddNode(this TreeList treelist, object val, bool isCheck, TreeListNode parentnode)
        {
            return treelist.AddNode(new object[] { val.ToString(), val.ToString(), val }, isCheck, parentnode);
        }
        /// <summary>
        /// 根据id查找节点
        /// </summary>
        /// <param name="treelist">TreeList</param>
        /// <param name="id">id</param>
        /// <returns>节点</returns>
        public static TreeListNode GetNodeById(this TreeList treelist, string id)
        {
            foreach (TreeListNode node in treelist.Nodes)
            {
                if (node.GetID() == id)
                {
                    return node;
                }
            }
            return null;
        }
        /// <summary>
        /// 获取节点值
        /// </summary>
        /// <param name="treelistnode">TreeListNode</param>
        /// <returns>节点</returns>
        public static object GetValue(this TreeListNode treelistnode)
        {
            return treelistnode.GetValue(2);
        }
        /// <summary>
        /// 获取节点ID
        /// </summary>
        /// <param name="treelistnode">TreeListNode</param>
        /// <returns>节点</returns>
        public static string GetID(this TreeListNode treelistnode)
        {
            return treelistnode.GetValue(0).ToString();
        }
        /// <summary>
        /// 获取节点显示文本
        /// </summary>
        /// <param name="treelistnode">TreeListNode</param>
        /// <returns>节点</returns>
        public static string GetCaption(this TreeListNode treelistnode)
        {
            return treelistnode.GetValue(1).ToString();
        }
    }
}
