﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraBars;

namespace XYZShell.CsHelper.DevExpressCore.Framework
{
    interface IBarItemCreaterable
    {
        BarItem Create(string xml);
    }
}
