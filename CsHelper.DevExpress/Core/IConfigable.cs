﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XYZShell.CsHelper.DevExpressCore.Core
{
    interface IConfigable
    {
        object Read();
        void Write(object obj);
    }
}
