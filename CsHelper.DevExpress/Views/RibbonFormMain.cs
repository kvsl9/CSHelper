﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;

namespace XYZShell.CsHelper.DevExpressCore.Views
{
    public partial class RibbonFormMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public RibbonFormMain()
        {
            DevExpress.UserSkins.BonusSkins.Register();
            //DevExpress.UserSkins.OfficeSkins.Register();
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle("Office 2010 Blue");
            InitializeComponent();
            init();
        }
        private void init()
        {
            DevExpress.XtraBars.Helpers.SkinHelper.InitSkinGallery(rgrSkin, true);
        }
        public void DisplayForm(Form form)
        {
            form.MdiParent = this;
            form.Show();
        }

        private void btnSetUi_ItemClick(object sender, ItemClickEventArgs e)
        {
            SetMainFormUI setuiform = new SetMainFormUI();
            setuiform.StartPosition = FormStartPosition.CenterParent;
            setuiform.ShowDialog();
        }
        
    }
}