﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XYZShell.CsHelper.DevExpressCore.Views;

namespace DevExpressControlTest
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();

            Load += FormMain_Load;
        }

        void FormMain_Load(object sender, EventArgs e)
        {
            this.Hide();
            RibbonFormMain mainform = new RibbonFormMain();
            mainform.Show();
            TreeListTestForm ttf = new TreeListTestForm();
            mainform.DisplayForm(ttf);            
        }
    }
}
