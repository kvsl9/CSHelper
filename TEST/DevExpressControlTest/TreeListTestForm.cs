﻿using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XYZShell.CsHelper.DevExpressCore.Helper;

namespace DevExpressControlTest
{
    public partial class TreeListTestForm : Form
    {
        public TreeListTestForm()
        {
            InitializeComponent();
            treeList1.InitNormal();
        }

        private void TreeListTestForm_Load(object sender, EventArgs e)
        {
            TreeListNode root1 = treeList1.AddNode("root1","root",1,null);
            treeList1.AddNode("y1","y",2,root1);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            TreeListNode fou = treeList1.FocusedNode;
            string id = fou.GetID();
            string caption = fou.GetCaption();
            object val = fou.GetValue();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            TreeListNode tn = treeList1.GetNodeById("root1");
        }
    }
}
