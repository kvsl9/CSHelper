﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace XYZShell.CsHelper.ExtendFunction
{
    public static class StringHelper
    {
        public static byte[] GetBytes(this string val,Encoding encoding)
        {
            return encoding.GetBytes(val);
        }
        public static byte[] GetBytes(this string val)
        {
            return GetBytes(val,Encoding.UTF8);
        }
        public static object ToObj(this string val,Type type)
        {
            TypeCode tc = Type.GetTypeCode(type);
            switch (tc)
            {
                case TypeCode.Boolean:
                    return bool.Parse(val);
                case TypeCode.Byte:
                    return byte.Parse(val);
                case TypeCode.Char:
                    return char.Parse(val);
                case TypeCode.DateTime:
                    return DateTime.Parse(val);
                case TypeCode.Decimal:
                    return decimal.Parse(val);
                case TypeCode.Double:
                    return double.Parse(val);
                case TypeCode.Int16:
                    return Int16.Parse(val);
                case TypeCode.Int32:
                    return int.Parse(val);
                case TypeCode.Int64:
                    return Int64.Parse(val);
                case TypeCode.SByte:
                    return sbyte.Parse(val);
                case TypeCode.Single:
                    return Single.Parse(val);
                case TypeCode.UInt16:
                    return UInt16.Parse(val);
                case TypeCode.UInt32:
                    return uint.Parse(val);
                case TypeCode.UInt64:
                    return UInt64.Parse(val);                    
                default:
                    throw new NotSupportedException("不支持的类型！");
            }
        }
        public static string MD5(this string val)
        {
            return SecurityHelper.MD5(val);
        }
        public static string SHA1(this string val)
        {
            return SecurityHelper.SHA1(val);
        }

    }
}
