﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XYZShell.CsHelper
{
    /// <summary>
    /// 页面列表
    /// </summary>
    /// <typeparam name="T">数据类型</typeparam>
    public class PageList<T>
    {
        /// <summary>
        /// 当前页码
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 每页数据大小
        /// </summary>
        public uint PageSize { get; set; }
        /// <summary>
        /// 总共页数
        /// </summary>
        public uint PageCount { get; set; }
        /// <summary>
        /// 数据总共条数
        /// </summary>
        public ulong DataSize { get; set; }
        /// <summary>
        /// 每页数据
        /// </summary>
        public List<T> ViewModes { get; set; }
    }
}
