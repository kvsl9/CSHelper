﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XYZShell.Helper
{
    public static class CollectionHelper
    {
		/// <summary>
		/// 元素不存在是添加
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="c"></param>
		/// <param name="item"></param>
        public static void AddIfNotExist<T>(this ICollection<T> c, T item)
        {
            if (!c.Contains(item))
            {
                c.Add(item);
            }
        }

		/// <summary>
		/// 元素不存在是添加
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="c"></param>
		/// <param name="item">要添加的元素</param>
		/// <param name="selector">选择用于比较的元素</param>
		public static void AddIfNotExist<T,V>( this ICollection<T> c, T item ,Func<T,V> selector)
		{
			if (!c.Contains( item,XYZShell.Helper.Equality<T>.CreateComparer(selector) ))
			{
				c.Add( item );
			}
		}

		/// <summary>
		/// 元素不存在是添加
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="c"></param>
		/// <param name="item">要添加的元素</param>
		/// <param name="comparer">相等比较</param>
		public static void AddIfNotExist<T>( this ICollection<T> c, T item, IEqualityComparer<T> comparer )
		{
			if (!c.Contains( item,comparer))
			{
				c.Add( item );
			}
		}

    }
}
