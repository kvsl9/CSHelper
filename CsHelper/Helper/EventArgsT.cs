﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XYZShell.Helper
{
	/// <summary>
	/// 通用事件参数
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class EventArgs<T>:EventArgs
	{
		/// <summary>
		/// 事件传递的参数
		/// </summary>
		public T Arg
		{
			get;
			set;
		}
	}

	/// <summary>
	/// 通用事件参数
	/// </summary>
	/// <typeparam name="T1"></typeparam>
	/// <typeparam name="T2"></typeparam>
	public class EventArgs<T1,T2> : EventArgs
	{
		/// <summary>
		/// 事件传递的参数1
		/// </summary>
		public T1 Arg1
		{
			get;
			set;
		}

		/// <summary>
		/// 事件传递的参数2
		/// </summary>
		public T2 Arg2
		{
			get;
			set;
		}
	}
}
