﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO.Compression;
using Microsoft.Win32;
namespace XYZShell.Helper
{
    public static class WebHelper
    {
		/// <summary>
		/// 下载为http地址为字符串(UTF8编码)
		/// </summary>
		/// <param name="uri"></param>
		/// <returns></returns>
        public static string DownloadString(this Uri uri)
        {
			return DownloadString( uri, Encoding.UTF8 );
        }
		/// <summary>
		/// 下载为http地址为字符串
		/// </summary>
		/// <param name="uri"></param>
		/// <param name="encoding">编码格式</param>
		/// <returns></returns>
        public static string DownloadString(this Uri uri,Encoding encoding)
        {
            WebClient c = new WebClient();
            c.Encoding = encoding;
            return c.DownloadString(uri);
        }

		/// <summary>
		/// 下载为http地址为字节数组
		/// </summary>
		/// <param name="uri"></param>
		/// <returns></returns>
        public static byte[] DownloadBytes(this Uri uri)
        {
            return new WebClient().DownloadData(uri);
        }

        public static string GetContentTypeByExt(string extWithDot)
        {
            const string DEFAULT_CONTENT_TYPE = "application/unknown";

            RegistryKey regkey, fileextkey;
            string filecontenttype, fileextension;

            //the file extension to lookup
            fileextension = extWithDot;

            try
            {
                //look in HKCR
                regkey = Registry.ClassesRoot;

                //look for extension
                fileextkey = regkey.OpenSubKey(fileextension);

                //retrieve Content Type value
                filecontenttype = fileextkey.GetValue("Content Type", DEFAULT_CONTENT_TYPE).ToString();

                //cleanup
                fileextkey = null;
                regkey = null;
            }
            catch
            {
                filecontenttype = DEFAULT_CONTENT_TYPE;
            }

            return filecontenttype;
            

        }
    }
}
