﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
#if NetFx40
using System.Diagnostics.Contracts;
#endif
using System.Data;
namespace XYZShell.Helper
{
	public static class WinFormHelper
	{
		/// <summary>
		/// 以check的值设置所有项的Checked属性
		/// </summary>
		/// <param name="lv"></param>
		/// <param name="check"></param>
		public static void AllCheck( this ListView lv, bool check )
		{
			if (lv==null)
			{
				return;
			}
			foreach (ListViewItem lvi in lv.Items)
			{
				lvi.Checked = check;
			}
		}

		/// <summary>
		/// 以select的值设置所有项的Selected属性
		/// </summary>
		/// <param name="lv"></param>
		/// <param name="select"></param>
		public static void AllSelect(this ListView lv,bool select)
		{
			foreach (ListViewItem lvi in lv.Items)
			{
				lvi.Selected = select;
			}
		}
		/// <summary>
		/// 设置所有项的Selected属性为true
		/// </summary>
		/// <param name="lv"></param>
		public static void SelectAll( this ListView lv )
		{
			foreach (ListViewItem lvi in lv.Items)
			{
				lvi.Selected = true;
			}
		}

		/// <summary>
		/// 设置所有项的Selected属性为false
		/// </summary>
		/// <param name="lv"></param>
		public static void UnSelectAll( this ListView lv )
		{
			foreach (ListViewItem lvi in lv.Items)
			{
				lvi.Selected = false;
			}
		}
		/// <summary>
		/// 反选
		/// </summary>
		/// <param name="lv"></param>
		public static void ReverseSelect( this ListView lv )
		{
			foreach (ListViewItem lvi in lv.Items)
			{
				lvi.Selected = !lvi.Selected;
			}
		}

		/// <summary>
		/// 所有项Checked属性设置为true 
		/// </summary>
		/// <param name="lv"></param>
		public static void CheckAll( this ListView lv )
		{
			foreach (ListViewItem lvi in lv.Items)
			{
				lvi.Checked = true;
			}
		}
		/// <summary>
		/// 所有项Checked属性设置为false
		/// </summary>
		/// <param name="lv"></param>
		public static void UnCheckAll( this ListView lv )
		{
			foreach (ListViewItem lvi in lv.Items)
			{
				lvi.Checked = false;
			}
		}
		/// <summary>
		/// 反选
		/// </summary>
		/// <param name="lv"></param>
		public static void ReverseCheck( this ListView lv )
		{
			foreach (ListViewItem lvi in lv.Items)
			{
				lvi.Checked = !lvi.Checked;
			}
		}
		/// <summary>
		/// 使用枚举器填充下拉项
		/// </summary>
		/// <typeparam name="T">枚举器类型</typeparam>
		/// <param name="cb"></param>
		/// <param name="clear">是否清除已有数据</param>
		public static void FillItems<T>( this ComboBox cb, IEnumerable<T> items, bool clear = true )
		{
			if (clear)
				cb.Items.Clear();
			foreach (var i in items)
			{
				cb.Items.Add( i );
			}
		}

		/// <summary>
		/// 使用枚举类型填充下拉项
		/// </summary>
		/// <typeparam name="T">一个枚举类型</typeparam>
		/// <param name="cb"></param>
		/// <param name="clear">是否清除已有数据</param>
		public static void FillItems<T>( this ComboBox cb, bool clear = true )
		{
			Type enumType = typeof( T );
#if NetFx40
			Contract.Assert( enumType.IsEnum ); 
#endif
			if (clear)
				cb.Items.Clear();

			foreach (var i in Enum.GetNames( enumType ))
			{
				cb.Items.Add( i );
			}
		}

		/// <summary>
		/// 绑定DataTable数据到listview
		/// </summary>
		/// <param name="lv"></param>
		/// <param name="data">待填充DataTable</param>
		/// <param name="hasCol">是否用DataTable的列替换</param>
		/// <param name="saveVauleToTag">是否报DataTable的数据写入到Tag属性</param>
		/// <param name="trasform">项文本转换函数,传入参数分别为从0开始的行号,从0开始的列号,数据本身</param>
		/// <param name="clearExistItems">清除已有的项</param>
		public static void FillData( this ListView lv, DataTable data, bool hasCol = false, bool saveVauleToTag = true,
			Func<int, int, object, string> trasform = null, bool clearExistItems = true )
		{
			if (clearExistItems)
			{
				lv.Items.Clear();
			}
			if (hasCol)
			{
				lv.Columns.Clear();
				foreach (DataColumn col in data.Columns)
				{
					lv.Columns.Add( col.Caption );
				}
			}
			int rownum = 0;
			foreach (DataRow row in data.Rows)
			{
				string item_text = trasform == null ? row[0].ToString() : trasform( rownum, 0, row[0] );
				ListViewItem item = new ListViewItem( item_text );
				if (saveVauleToTag)
					item.Tag = row[0];
				for (int i = 1; i < data.Columns.Count; i++)
				{
					string subitem_text = trasform == null ? row[i].ToString() : trasform( rownum, i, row[i] );
					item.SubItems.Add( subitem_text );
					if (saveVauleToTag)
						item.SubItems[i].Tag = row[i];

				}
				rownum++;
				lv.Items.Add( item );
			}
		}


		/// <summary>
		/// 转换ListView数据为DataTable
		/// </summary>
		/// <param name="lv"></param>
		/// <param name="hasCol">是否保存ListView列为DataTable的列</param>
		/// <param name="valueInTag">是否从ListView对应Item的Tag属性读取数据而不是读取Text值</param>
		/// <returns></returns>
		public static DataTable ToDataTable( this ListView lv, bool hasCol = true, bool valueInTag = false )
		{

			DataTable table = new DataTable();
			if (hasCol)
			{
				foreach (ColumnHeader col in lv.Columns)
				{
					table.Columns.Add( col.Text );
				}
			}
			foreach (ListViewItem item in lv.Items)
			{
				DataRow row = table.NewRow();
				List<object> rowdata = new List<object>();
				if (!hasCol)
				{
					table.Columns.Add( item.ToString() );
				}

				foreach (ListViewItem.ListViewSubItem it in item.SubItems)
				{
					rowdata.Add( valueInTag ? it.Tag : it.Text );
				}
				object[] dataarry = rowdata.ToArray();
				row.ItemArray = dataarry;
				table.Rows.Add( row );
			}
			return table;
		}
	}
}
