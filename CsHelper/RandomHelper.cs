﻿/*
 * Author: XYZ.SEAN.M.THX 
 * Email: seanmpthx@gmail.com
 * Copyright: (c) 2010-2020
 * Version:0.1
 * 
 * <Update>
 *   2012-07-04 XYZ.SEAN.M.THX 创建 Version 0.1
 * </Update>
 * <Remark>
 * 
 * </Remark>
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XYZShell.CsHelper
{
	/// <summary>
	/// 产生随机的各种数据的相关帮助类
	/// </summary>
	public class RandomHelper
	{
		private static readonly string randomString = "1234567890QWERTYUIOPASDFGHJKLZXCVBNM";
		/// <summary>
		/// 得到随机不重复的随机数
		/// </summary>
		/// <param name="amount">随机数个数</param>
		/// <param name="min">最小值</param>
		/// <param name="max">最大值</param>
		/// <returns></returns>
		public static List<int> GetRandom(int amount, int min, int max)
		{
			if ((max - min) < amount)
			{
				throw new Exception("无法产生那么多不重复的随机数");
			}
			int i = 0;
			Random rd;
			List<int> lints = new List<int>();
			while (true)
			{
				rd = new Random(Guid.NewGuid().GetHashCode());
				int teint = rd.Next(max - min) + min;
				if (!lints.Contains(teint))
				{
					lints.Add(teint);
					i++;
				}
				if (i == amount)
				{
					break;
				}
			}
			return lints;
		}
		/// <summary>
		/// 得到随机不重复的随机数
		/// </summary>
		/// <param name="amount">随机数个数</param>
		/// <returns>0.0-1.0</returns>
		public static List<double> GetRandom(int amount)
		{
			int i = 0;
			Random rd;
			List<double> lints = new List<double>();
			while (true)
			{
				rd = new Random(Guid.NewGuid().GetHashCode());
				double d = rd.NextDouble();
				if (!lints.Contains(d))
				{
					lints.Add(d);
					i++;
				}
				if (i == amount)
				{
					break;
				}
			}
			return lints;
		}
		/// <summary>
		/// 产生固定长度的随机数字和大写字母（主要用于验证码中的随机数据）
		/// </summary>
		/// <param name="length">长度</param>
		/// <returns></returns>
		public static string GetRandomString(int length)
		{
			StringBuilder sb = new StringBuilder();
			Random rd = new Random();
			for (int i = 0; i < length; i++)
			{
				sb.Append(randomString[rd.Next(0,randomString.Length)]);
			}
			return sb.ToString();
		}
	}
}
