﻿/*
 * Author: XYZ.SEAN.M.THX 
 * Email: seanmpthx@gmail.com
 * Copyright: (c) 2010-2020
 * Version:0.2
 * 
 * <Update>
 *   2012-06-05 XYZ.SEAN.M.THX 添加配置文件AppSettings配置节的增删改查 Version 0.2
 *   2012-06-04 XYZ.SEAN.M.THX 创建 Version 0.1
 * </Update>
 * <Remark>
 * 
 * </Remark>
 */

using System;
using System.Configuration;
using System.Collections.Generic;

namespace XYZShell.CsHelper.ConfigHelper
{
    /// <summary>
    /// NET配置帮助类
    /// </summary>
    public class ConfigHelp
    {
        static AppSettingsReader ar = new AppSettingsReader();
        /// <summary>
        /// 获取AppSettingKey值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="type">key值类型</param>
        /// <returns>object 可转换为type</returns>
        public static object GetAppSettingValue(string key, Type type)
        {
            return ar.GetValue(key, type);
        }
        /// <summary>
        /// 获取AppSettingKey值
        /// </summary>
        /// <param name="key"></param>
        /// <returns>string</returns>
        public static string GetAppSettingValue(string key)
        {
            return (string)ar.GetValue(key, typeof(string));
        }
        /// <summary>
        /// 添加AppSetting设置
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void AddAppSetting(string key, string value)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            AddAppSetting(config, key, value);
        }
        /// <summary>
        /// 添加AppSetting设置
        /// </summary>
        /// <param name="filepath">文件路径</param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void AddAppSetting(string filepath, string key, string value)
        {
            Configuration configfile = ConfigurationManager.OpenExeConfiguration(filepath);
            AddAppSetting(configfile,key,value);
        }
        /// <summary>
        /// 添加AppSetting设置
        /// </summary>
        /// <param name="_config">配置文件类<see cref="System.Configuration.Configuration"/></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void AddAppSetting(Configuration _config, string key, string value)
        {
            AppSettingsSection appsetting = _config.AppSettings;
            appsetting.Settings.Add(key,value);
            _config.Save(ConfigurationSaveMode.Modified);
        }
        /// <summary>
        /// 更新AppSetting设置
        /// </summary>
        /// <param name="_config">配置文件类<see cref="System.Configuration.Configuration"/></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void UpdateAppSetting(Configuration _config,string key,string value)
        {
            AppSettingsSection appsetting = _config.AppSettings;
            appsetting.Settings[key].Value = value;
            _config.Save(ConfigurationSaveMode.Modified);
        }
        /// <summary>
        /// 更新AppSetting设置
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void UpdateAppSetting(string key,string value)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            UpdateAppSetting(config,key,value);
        }
        /// <summary>
        /// 更新AppSetting设置
        /// </summary>
        /// <param name="filepath">配置文件路径</param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void UpdateAppSetting(string filepath,string key,string value)
        {
            Configuration configfile = ConfigurationManager.OpenExeConfiguration(filepath);
            UpdateAppSetting(configfile, key, value);
        }
        /// <summary>
        /// 删除AppSetting设置
        /// </summary>
        /// <param name="_config">配置文件类<see cref="System.Configuration.Configuration"/></param>
        /// <param name="key"></param>
        public static void DelAppSetting(Configuration _config,string key)
        {
            AppSettingsSection appsetting = _config.AppSettings;
            appsetting.Settings.Remove(key);
            _config.Save(ConfigurationSaveMode.Modified);
        }
        /// <summary>
        /// 删除AppSetting设置
        /// </summary>
        /// <param name="key"></param>
        public static void DelAppSetting(string key)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            DelAppSetting(config,key);
        }
        /// <summary>
        /// 删除AppSetting设置
        /// </summary>
        /// <param name="filepath">配置文件路径</param>
        /// <param name="key"></param>
        public static void DelAppSetting(string filepath,string key)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(filepath);
            DelAppSetting(config, key);
        }
        public static ConnectionStringSettings GetConStrSeting(string kname)
        {           
           return ConfigurationManager.ConnectionStrings[kname];
        }
        public static void GetConStrSeting(string kname, out string ProviderName, out string ConnectionString)
        {
            ConnectionStringSettings css = GetConStrSeting(kname);          
            ProviderName = css.ProviderName;
            ConnectionString = css.ConnectionString;
        }
    }
}
