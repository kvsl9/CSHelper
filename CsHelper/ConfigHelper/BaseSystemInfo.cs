﻿/*
 * Author: XYZ.SEAN.M.THX 
 * Email: seanmpthx@gmail.com
 * Copyright: (c) 2010-2020
 * Version:0.1
 * 
 * <Update>
 *   创建于 2012-06-01
 * </Update>
 * <Remark>
 * 
 * </Remark>
*/

using System;

namespace XYZShell.CsHelper.ConfigHelper
{
    /// <summary>
    /// 系统基本配置信息基类
    /// </summary>
    public class BaseSystemInfo
    {
        /// <summary>
        /// 应用系统ID
        /// </summary>
        public static string SysId;
        /// <summary>
        /// 软件全称
        /// </summary>
        public static string SoftFullName;
        /// <summary>
        /// 软件版本
        /// </summary>
        public static string Version;
        /// <summary>
        /// 公司名称
        /// </summary>
        public static string CompanyName;
        /// <summary>
        /// 公司联系电话
        /// </summary>
        public static string CompanyPhone;
        /// <summary>
        /// 版权声明
        /// </summary>
        public static string Copyright;
    }

}