﻿/*
 * Author: XYZ.SEAN.M.THX 
 * Email: seanmpthx@gmail.com
 * Copyright: (c) 2010-2020
 * Version:0.1
 * 
 * <Update>
 *   创建于 2012-06-01
 * </Update>
 * <Remark>
 * 
 * </Remark>
*/

using System;
using Microsoft.Win32;
using System.Text;

namespace XYZShell.CsHelper.ConfigHelper
{
    /// <summary>
    /// 一个简单的注册表操作类
    /// </summary>
   public class RegeditHelp
    {
       protected static string SubKey;
       /// <summary>
       /// 设置
       /// </summary>
       /// <param name="subKey">注册表子项</param>
       public static void SetSubKey(string subKey)
       {
           SubKey = subKey;
       }
       /// <summary>
       /// 获取注册表KEY值
       /// </summary>
       /// <param name="key">键</param>
       /// <returns>值</returns>
       public static string GetValue(string key)
       {
           if (string.IsNullOrEmpty(SubKey))
           {
               throw new Exception("请先调用SetSubKey()设置SubKey值");
           }
           RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(SubKey, false);
           return (string)registryKey.GetValue(key);
       }
       /// <summary>
       /// 设置注册表
       /// </summary>
       /// <param name="key">键</param>
       /// <param name="keyValue">值</param>
       public static void SetValue(string key, string keyValue)
       {
           if (string.IsNullOrEmpty(SubKey))
           {
               throw new Exception("请先调用SetSubKey()设置SubKey值");
           }
           RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(SubKey, true);
           registryKey.SetValue(key, keyValue);
       }
       /// <summary>
       /// 判断一个注册表项是否存在
       /// </summary>
       /// <param name="key">键</param>
       /// <returns>存在返回TRUE</returns>
       public static bool Exists(string key)
       {
           if (string.IsNullOrEmpty(SubKey))
           {
               throw new Exception("请先调用SetSubKey()设置SubKey值");
           }
           bool returnValue = false;
           RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(SubKey, false);
           string[] SubKeyNames = registryKey.GetSubKeyNames();
           for (int i = 0; i < SubKeyNames.Length; i++)
           {
               if (key.Equals(SubKeyNames[i]))
               {
                   returnValue = true;
                   break;
               }
           }
           return returnValue;
       }
   }
}
