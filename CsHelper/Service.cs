﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XYZShell.CsHelper
{
    public class Service
    {
        Dictionary<Type, object> kv = new Dictionary<Type, object>();
        public void AddService(Type key, object val)
        {
            if (kv.ContainsKey(key))
            {
                kv[key] = val;
            }
            else
            {
                kv.Add(key,val);
            }
        }
        public object GetService(Type key)
        {
            if (kv.ContainsKey(key))
            {
                return kv[key];
            }
            return null;
        }
        public T GetService<T>()
        {
            return (T)GetService(typeof(T));
        }
        public void RemoveService(Type key)
        {
            if (kv.ContainsKey(key))
            {
                kv.Remove(key);
            }
        }
    }
}
