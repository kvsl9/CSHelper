﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.Caching;//net4.0后的版本才支持
using System.Collections;

namespace XYZShell.CsHelper
{
#if NET4
    public class xx 
    {
    
    }
#endif
    /// <summary>
    /// 缓存辅助类
    /// </summary>
    public static class Cacher
    {
        static readonly ObjectCache cache = MemoryCache.Default;
        /// <summary>
        /// 判断缓存中是否存在key值对应的对象
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool Exists(string key)
        {
            return cache.Contains(key) && cache.Get(key) != null;           
        }
        /// <summary>
        /// 获取缓存中的所有key
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAll()
        {
            return cache.Select(KeyValuePair => KeyValuePair.Key).ToList();
        }
        /// <summary>
        /// 移除缓存中的对象
        /// </summary>
        /// <param name="key"></param>
        public static void Remove(string key)
        {
            cache.Remove(key);
        }
        /// <summary>
        /// 根据key值获取缓存中的对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Get<T>(string key) where T:class
        {
            try
            {
                return (T)cache[key];
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 根据key值获取缓存中的对象
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object Get(string key)
        {
            try
            {
                return cache[key];
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 添加对象到缓存，如果存在则重新设置缓存中的对象
        /// </summary>
        /// <param name="key"></param>
        /// <param name="val"></param>
        /// <param name="dtof"></param>
        public static void AddOrSet(string key,object val,DateTimeOffset dtof)
        {
            if (Exists(key))
            {
                cache[key] = val;
            }
            else
            {
                cache.Add(key, val, dtof);
            }
        }
        /// <summary>
        ///  添加对象到缓存，如果存在则重新设置缓存中的对象
        /// </summary>
        /// <param name="key"></param>
        /// <param name="val"></param>
        public static void AddOrSet(string key, object val)
        {
            AddOrSet(key,val,DateTime.Now.AddDays(1));
        }
        /// <summary>
        /// 尝试获取缓存中的对象，如果不存在则添加一个对象到缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="P"></typeparam>
        /// <param name="key"></param>
        /// <param name="param"></param>
        /// <param name="GetObjToCache"></param>
        /// <param name="dtof"></param>
        /// <returns></returns>
        public static T TryGetOrAdd<T, P>(string key,P param,Func<P,T> GetObjToCache,DateTimeOffset dtof) where T:class
        {
            if (Exists(key))
            {
                return Get<T>(key);
            }
            else
            {
                T obj = GetObjToCache(param);
                AddOrSet(key,obj,dtof);
                return obj;
            }
        }
        /// <summary>
        /// 尝试获取缓存中的对象，如果不存在则添加一个对象到缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="P"></typeparam>
        /// <param name="key"></param>
        /// <param name="param"></param>
        /// <param name="GetObjToCache"></param>
        /// <returns></returns>
        public static T TryGetOrAdd<T, P>(string key,P param,Func<P,T> GetObjToCache) where T:class
        {
            return TryGetOrAdd<T, P>(key,param,GetObjToCache,DateTime.Now.AddDays(1));
        }
    }
    /// <summary>
    /// 简单的缓存辅助类，暂不支持缓存过期等缓存管理
    /// </summary>
    public static class Cacher20
    {
        static readonly Hashtable cache = null;
        static Cacher20()
        {
            if (cache==null)
            {
                cache = new Hashtable();
            }
        }
        /// <summary>
        ///  判断缓存中是否存在key值对应的对象
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool Exists(string key)
        {
            return cache.ContainsKey(key) && cache[key] != null;
        }
        /// <summary>
        ///  获取缓存中的所有key
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAll()
        {
            List<string> allkeys = new List<string>();
            foreach (string item in cache.Keys)
            {
                allkeys.Add(item);
            }
            return allkeys;
        }
        /// <summary>
        /// 移除缓存中的对象
        /// </summary>
        /// <param name="key"></param>
        public static void Remove(string key)
        {
            cache.Remove(key);
        }
        /// <summary>
        /// 根据key值获取缓存中的对象
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object Get(string key)
        {
            if (Exists(key))
            {
                return cache[key];
            }
            return null;
        }
        /// <summary>
        /// 根据key值获取缓存中的对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Get<T>(string key)
        {
            if (Exists(key))
            {
                return (T)cache[key];
            }
            return default(T);
        }
        /// <summary>
        /// 添加对象到缓存，如果存在则重新设置缓存中的对象
        /// </summary>
        /// <param name="key"></param>
        /// <param name="val"></param>
        public static void AddOrSet(string key,object val)
        {
            if (cache.ContainsKey(key))
            {
                cache[key] = val;
            }
            else
            {
                cache.Add(key,val);
            }
        }
        /// <summary>
        /// 尝试获取缓存中的对象，如果不存在则添加一个对象到缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="P"></typeparam>
        /// <param name="key"></param>
        /// <param name="param"></param>
        /// <param name="GetObjToCache"></param>
        /// <returns></returns>
        public static T TryGetOrAdd<T, P>(string key, P param,Func<P,T> GetObjToCache) where T : class
        {
            if (Exists(key))
            {
                return Get<T>(key);
            }
            else
            {
                T obj = GetObjToCache(param);
                AddOrSet(key, obj);
                return obj;
            }
        }
    }
}
