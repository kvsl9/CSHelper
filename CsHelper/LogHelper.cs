﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
namespace XYZShell.CsHelper
{
    public class LogHelper
    {
        static log4net.ILog log = null;
        static log4net.ILog logError = null;
        static log4net.ILog logDebug = null;
        static log4net.ILog logFatal = null;
        static log4net.ILog logWarn = null;
        static LogHelper()
        {
            log = log4net.LogManager.GetLogger("loginfo");
            logError = log4net.LogManager.GetLogger("logerror");
            logDebug = log4net.LogManager.GetLogger("logdebug");
            logFatal = log4net.LogManager.GetLogger("logfatal");
            logWarn = log4net.LogManager.GetLogger("logwarn");
        }
        public static void Debug(object message, Exception ex)
        {
            if (logDebug != null && logDebug.IsDebugEnabled)
            {                
                logDebug.Debug(message, ex);
               // log4net.Core.LoggingEvent logev = new log4net.Core.LoggingEvent()                
            }            
        }
        public static void Error(object message, Exception ex)
        {
            if (logError != null && logError.IsErrorEnabled)
            {
                logError.Error(message, ex);
            }
        }
        public static void Fatal(object message, Exception ex)
        {
            if (logFatal != null && logFatal.IsFatalEnabled)
            {
                logFatal.Fatal(message, ex);
            }
        }
        public static void Info(object message, Exception ex)
        {
            if (log != null && log.IsInfoEnabled)
            {
                log.Info(message, ex);
            }
        }
        public static void Warn(object message, Exception ex)
        {
            if (logWarn != null && logWarn.IsWarnEnabled)
            {
                logWarn.Warn(message, ex);
            }
        }
      
    }
}
