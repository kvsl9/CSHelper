﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XYZShell.CsHelper.DevExpressCore.Views;

namespace XYZShell.Launcher
{
    public partial class LoadForm : Form
    {
        public LoadForm()
        {
            InitializeComponent();
        }

        private void LoadForm_Load(object sender, EventArgs e)
        {
            this.Hide();
            RibbonFormMain mainForm = new RibbonFormMain();
            mainForm.Show();           
        }
    }
}
