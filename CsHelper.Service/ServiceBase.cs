﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XYZShell.CsHelper.Service
{
    public class ServiceBase
    {
        public virtual void Init()
        { }
        public virtual void Start()
        { }
        public virtual void Stop()
        { }
    }
}
