﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.PeerToPeer;
using System.Net.PeerToPeer.Collaboration;
using System.Net;
using System.ServiceModel;

namespace XYZShell.CsHelper.Service
{
    public class P2PHelper<T> : ServiceBase, IDisposable
    {
        public string GUIDName { get; set; }
        public string Port { get; set; }
        public object ServiceObj { get; set; }
        public Type IServiceType { get; set; }
        public Dictionary<string, T> P2PService { get; set; }
        public bool RefreshEnded { get; set; }

        private string machineName;
        private string serviceUrl = null;
        private ServiceHost host;
        private PeerName peerName;
        private PeerNameRegistration peerNameRegistration;

        public override void Init()
        {
            GUIDName = Guid.NewGuid().ToString();
            machineName = Environment.MachineName;
            foreach (IPAddress address in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (address.AddressFamily == 
                    System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    serviceUrl = string.Format("net.tcp://{0}:{1}/P2PService",address,Port);
                    break;
                }
            }
            host = new ServiceHost(this.ServiceObj,new Uri(this.serviceUrl));
            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.None;
            host.AddServiceEndpoint(IServiceType, binding, this.serviceUrl);
        }
        public override void Start()
        {
            if (serviceUrl == null)
            {
                throw new ArgumentNullException("ServiceURL", "不能为空");
            }
            if (ServiceObj == null)
            {
                throw new ArgumentNullException("ServiceObj", "不能为空");
            }
            if (IServiceType == null)
            {
                throw new ArgumentNullException("IServiceObj", "不能为空");
            }
            if (host == null)
            {
                throw new ArgumentNullException("host", "不能为空");
            }
            host.Open();
            peerName = new PeerName(GUIDName, PeerNameType.Unsecured);
            peerNameRegistration = new PeerNameRegistration(peerName, int.Parse(this.Port));
            peerNameRegistration.Cloud = Cloud.AllLinkLocal;
            peerNameRegistration.Start();
            Refresh();

        }
        public override void Stop()
        {
            if (peerNameRegistration!=null)
            {
                peerNameRegistration.Stop();
            }
            if (host!=null)
            {
                host.Close();
            }           
        }
        public void Refresh()
        {
            PeerNameResolver resolver = new PeerNameResolver();
            resolver.ResolveProgressChanged += new EventHandler<ResolveProgressChangedEventArgs>(resolver_ResolveProgressChanged);
            resolver.ResolveCompleted += new EventHandler<ResolveCompletedEventArgs>(resolver_ResolveCompleted);
            if (P2PService!=null)
            {
                P2PService.Clear();    
            }
            resolver.ResolveAsync(new PeerName(this.GUIDName),1);
            RefreshEnded = false;
        }

        void resolver_ResolveCompleted(object sender, ResolveCompletedEventArgs e)
        {
            RefreshEnded = true;
        }

        void resolver_ResolveProgressChanged(object sender, ResolveProgressChangedEventArgs e)
        {
            PeerNameRecord peer = e.PeerNameRecord;
            foreach (IPEndPoint ep in peer.EndPointCollection)
            {
                if (ep.Address.AddressFamily == 
                    System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    string endpointUrl = string.Format("net.tcp://{0}:{1}/P2PService",ep.Address,ep.Port);
                    NetTcpBinding binding = new NetTcpBinding();
                    binding.Security.Mode = SecurityMode.None;
                    T serviceProxy = ChannelFactory<T>.CreateChannel(binding, new EndpointAddress(endpointUrl));
                    P2PService.Add(peer.PeerName.PeerHostName, serviceProxy);
                }
            }
        }

        public void Dispose()
        {

        }
    }
}
